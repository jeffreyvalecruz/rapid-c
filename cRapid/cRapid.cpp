/* cRapid Dynamic Link Library. (C++ Version)
* General purpose multi-instance-PID process handler.
*
* Usage:
* To be called periodical for output calculations,
* based on the input value and a given setpoint, 
* using P-I-D feedback evaluations.
*
* Author: Jeffrey Vale Cruz
* Date  : 29/7-2016
*
* */
#include <string>
#include "stdafx.h"
#include "PID.h"
#include "Log.h"
#include <map>

static std::map<std::string, PID> PIDRegistry;

bool CheckID(std::string ID) 
{
	return PIDRegistry.find(ID) != PIDRegistry.end();
}

PID GetPIDInstance(std::string ID)
{
	return PIDRegistry.find(ID)->second;
}

extern "C++" __declspec(dllexport) void __stdcall RegisterPID(char * ID, 
														      double SetPoint, 
	                                                          double OutputRangeMin, 
	                                                          double OutputRangeMax, 
	                                                          double KpGain, 
	                                                          double KiGain, 
	                                                          double KdGain)
{
	std::string strID = ID;
	PIDRegistry.insert(std::make_pair(ID, PID(strID, SetPoint, OutputRangeMin, OutputRangeMax, KpGain, KiGain, KdGain)));
}

/*
* 
* 
*/
extern "C++" __declspec(dllexport) double __stdcall PIDInOut(char * ID, double InputValue)
{
	std::string strID = ID;
	if (CheckID(strID))
	{
		return GetPIDInstance(strID).CalculateOutput(InputValue);
	}
	else
	{
		return NAN;
	}
}

extern "C++" __declspec(dllexport) double __stdcall PIDInsOut(char * ID, double InputValues[], double InputDivergencyLimit) 
{
	std::string strID = ID;
	if (CheckID(strID))
	{
		double min, max = InputValues[0];

		for (int i = 0; i < sizeof(InputValues); i++)
		{
			if (InputValues[i] < min)
			{
				min = InputValues[i];
			}
			else if (InputValues[i] > max)
			{
				max = InputValues[i];
			}
		}

		if (max - min > InputDivergencyLimit)
		{
			Log(Catagory::ERR, "{" + strID + "} Input divergency limit exceded.");
			return NAN;
		}
		else 
		{
			return GetPIDInstance(strID).CalculateOutput(max);
		}
	}
	else
	{
		Log(Catagory::ERR, "");
		return NAN;
	}
}





