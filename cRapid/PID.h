#pragma once
#include <string>

class PID
{
private:
	std::string m_ID;
	double m_setPoint;
	double m_outputRangeMin;
	double m_outputRangeMax;
	double m_KpGain;
	double m_KiGain;
	double m_KdGain;

	long   m_tickCount;

	double m_registry;
	double m_feedbackRegistry;

	double ElapsedSeconds(void);

public:
	PID(std::string ID,
		double SetPoint,
		double OutputRangeMin,
		double OutputRangeMax,
		double KpGain,
		double KiGain,
		double KdGain);

	double CalculateOutput(double InputValue);

	~PID();

};
