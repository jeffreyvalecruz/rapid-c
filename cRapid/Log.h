#pragma once
#include <string>

enum Catagory {INFO, WARNING, ERR};
static const std::string CatagoryStr[] = { "INFO", "WARNING", "ERROR" };

class Log
{
public:
	Log(Catagory Catagory, std::string LogText);
	~Log();
};
