#include "stdafx.h"
#include "PID.h"
#include "Log.h"


PID::PID(std::string ID,
		 double SetPoint,
		 double OutputRangeMin,
		 double OutputRangeMax,
		 double KpGain,
		 double KiGain,
		 double KdGain)
{
	m_ID = ID;
	m_setPoint = SetPoint;
	m_outputRangeMin = OutputRangeMin;
	m_outputRangeMax = OutputRangeMax;
	m_KpGain = KpGain;
	m_KiGain = KiGain;
	m_KdGain = KdGain;

	m_tickCount = GetTickCount();
	Log(Catagory::INFO, "Constructor called. PID instance initialized: {" + m_ID + "}");
}

PID::~PID()
{
}


double PID::ElapsedSeconds() 
{
	long elapsedTicks = GetTickCount() - m_tickCount;
	m_tickCount = GetTickCount();
	double elapsedSeconds = elapsedTicks / 1000;
	Log(Catagory::INFO, "{" + m_ID + "} Elapsed seconds since last call : " + std::to_string(elapsedSeconds));
	return elapsedSeconds;
}

double PID::CalculateOutput(double InputValue) 
{
	Log(Catagory::INFO, "{" + m_ID + "} PID calculation requested.");
	m_registry = m_feedbackRegistry;
	m_feedbackRegistry = m_setPoint - InputValue;

	double elapsedSeconds = ElapsedSeconds();

	double output = ((m_feedbackRegistry * m_KpGain) + (((m_feedbackRegistry * elapsedSeconds) +
					(m_registry * elapsedSeconds)) * m_KiGain) +
					((m_feedbackRegistry - m_registry) / elapsedSeconds) * m_KdGain);

	if (output < m_outputRangeMin) 
	{
		Log(Catagory::WARNING, "{" + m_ID + "} Output is coerced to: " + std::to_string(m_outputRangeMin));
		output = m_outputRangeMin;
	}
	else if (output > m_outputRangeMax) 
	{
		Log(Catagory::WARNING, "{" + m_ID + "} Output is coerced to: " + std::to_string(m_outputRangeMax));
		output = m_outputRangeMax;
	}

	Log(Catagory::INFO, "{" + m_ID + "} Output calculated to: " + std::to_string(output));
	return output;
}

